package com.endereco.cep.controller;

import com.endereco.cep.Client.Cep;
import com.endereco.cep.Service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CepController {
    @Autowired
    private CepService cepService;

    @GetMapping("/{cep}")
    public Cep create(@PathVariable String cep) {
        return cepService.getByCep(cep);
    }
}
