package com.endereco.usuario.Controller;

import com.endereco.usuario.Client.Cep;
import com.endereco.usuario.Client.CepClient;
import com.endereco.usuario.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {
    @Autowired
    private CepClient cepClient;


    @GetMapping("/{usuario}/{cep}")
    public Usuario create(@PathVariable String usuario, @PathVariable String cep) {
        Usuario usuarioNovo = new Usuario();
        usuarioNovo.setNome(usuario);
        usuarioNovo.setCep(cep);

        Cep byCep = cepClient.getByCep(cep);
        usuarioNovo.setBairro(byCep.getBairro());
        usuarioNovo.setLogradouro(byCep.getLogradouro());

        return usuarioNovo;
    }

}

