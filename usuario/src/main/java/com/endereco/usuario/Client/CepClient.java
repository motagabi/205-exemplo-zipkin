package com.endereco.usuario.Client;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CEP")
public interface CepClient {
    @GetMapping("/{cep}")
    Cep getByCep(@PathVariable String cep);
}